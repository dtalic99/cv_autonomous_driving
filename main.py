import cv2 as cv
# TODO: create your module
import ajavor
import igrulovic
import igrbas
import ismok
import fjurusic
import inemet
import mzalac

fjurusic_coords = [
    [327, 17, 447, 65],
    [625, 24, 737, 64],
    [880, 253, 912, 339],
]



# TODO: add full video path, and add locally compressed video files
cap = cv.VideoCapture('/Users/antejavor/VSCProjects/cv_autonomous_driving/resources/video/DayDrive1.mp4')
image = cv.imread("resources/fjurusic/DayDrive1_trim1_g.jpg")

grula_img = image.copy()
fjurusic_img = image.copy()

for coords in fjurusic_coords:
    cv.rectangle(grula_img, (coords[0], coords[1]), (coords[2], coords[3]), (0, 255, 0), 2)

cv.imshow("frame", grula_img)
cv.waitKey(0)

fjurusic.analyze_traffic_light(fjurusic_img, fjurusic_coords)
cv.imshow("frame", fjurusic_img)
cv.waitKey(0)

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        print("Can't receive frame. Exiting ...")
        break

    frame_analysis = frame.copy()

    # Detection and processing function for frame_analysis. If you need frame copy, create it in your function
    x, y = ajavor.detect_circle(frame_analysis)

    # TODO: add your detection function

    igrulovic.detect_traffic_light(frame_analysis, frame)


    # hardcoded coordinates to simulate sign detection
    signPrediction = igrbas.TrafficSignClassification.predict(frame_analysis, 890, 220, 65, 65)

    # Calling the function for detecting pedestrians
    ismok_results = ismok.detect_pedestrians(frame_analysis)

    # Detection of lines on road
    dturbeki_road_lines = dturbeki.detect_lines(frame)

    # Car detection
    mmorosavljevic_cars = mmorosavljevic.detect_cars(frame)


    # Drawing functions on basic frame
    ajavor.draw_circle(frame, x, y)

    # TODO: add your visualisation function
    igrbas.write_sign_text(frame, signPrediction, 965, 255)

    mrajic.detect_and_blur_faces(frame)
    # Calling the function for drawing pedestrian boxes
    ismok.draw_pedestrian_boxes(frame, ismok_results)

    # Drawing lines of road on basic frame
    dturbeki.draw_lines(frame, dturbeki_road_lines)

    mmorosavljevic.drawc_cars(frame, mmorosavljevic_cars)


    inemet.lanesDetection(frame)

    mzalac.mzalac_project()

    cv.imshow('frame', frame)
    if cv.waitKey(1) == ord('q'):
        break

cap.release()
cv.destroyAllWindows()
