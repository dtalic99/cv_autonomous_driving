import cv2 as cv
import numpy as np

# # algoritam za prepoznavanje prometnih traka
# # dijelovi algoritma :
# # 1. video se dekodira u slijed slika (frame)
# # 2. pretvorba slike iz RGB (BGR) u nijanse sive
# # 3. uz pomoć Canny Edge-a detektirati rubove na slikama
# # 4. stvara se maska i maskira se slika te se prikazuje područje interesa
# # 5. hough transform tool koji kao izlaz daje otkrivene linije

def lanesDetection(img):
    height = img.shape[0]
    width = img.shape[1]

    # definiranje područja koje nas zanima
    are_of_interest = [
        (0, height-100), (width / 2, height / 2 + 100), (width - 100, height - 100)
    ]
    # pretvaranje slike u sivu
    gray_img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)

    # detektiranje rubova na sivoj slici
    edge = cv.Canny(gray_img, 50, 100)
    cropped_img = region_of_interest(
        edge, np.array([are_of_interest], np.int32))

    # detektiranje linija
    lines = cv.HoughLinesP(cropped_img, rho=2, theta=np.pi / 180,
                           threshold=50, lines=np.array([]), minLineLength=10, maxLineGap=30)
    image_with_lines = draw_lines(img, lines)
    return image_with_lines

# funkcija za kreiranje i definiranje maske
def region_of_interest(img, vertices):
    mask = np.zeros_like(img)
    match_mask_color = (255)
    cv.fillPoly(mask, vertices, match_mask_color)
    masked_image = cv.bitwise_and(img, mask)
    return masked_image

# # funkcija koja prikazuje detektirane linije
def draw_lines(img, lines):
    img = np.copy(img)
    blank_image = np.zeros((img.shape[0], img.shape[1], 3), np.uint8)

    for line in lines:
        for x1, y1, x2, y2 in line:
            cv.line(blank_image, (x1, y1), (x2, y2), (0, 255, 0), 2)

    img = cv.addWeighted(img, 0.8, blank_image, 1, 0.0)
    return img

